<div class="hero-wrap" style="background-image: url('<?php the_field('background','options'); ?>');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
    <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
        <div class="col-md-12 ftco-animate">
        <h2 class="subheading"><?php the_field('title_1','options'); ?></h2>
        <h1 class="mb-4 mb-md-0"><?php the_field('title_2','options'); ?></h1>
        <div class="row">
            <div class="col-md-7">
                <div class="text">
                    <?php the_field('desc','options'); ?>
                    <div class="mouse">
                        <a href="#" class="mouse-icon">
                            <div class="mouse-wheel"><span class="ion-ios-arrow-round-down"></span></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>