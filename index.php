<?php get_header(); ?>

	<?php get_template_part('template-parts/hero');?>

	<?php if (have_posts()): while(have_posts()): the_post(); ?>
		<?php get_template_part('template-parts/post-home'); ?>
	<?php endwhile; endif; ?>

<?php get_footer(); ?>