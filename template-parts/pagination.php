<div class="row mt-5">
    <div class="col text-center">
                <?php the_posts_pagination([
				  'end_size' => 1,
				  'mid_size' => 1,
				  'prev_text' => '<',
				  'next_text' => '>',
				  'screen_reader_text' => ' '
			  ]); ?>
    </div>
</div>