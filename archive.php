<?php get_header(); ?>

	<?php get_template_part('template-parts/hero', 'archive');?>
	
   	<section class="ftco-section">
   		<div class="container">
   			<div class="row">
   				<div class="col-md-12">
					<?php if (have_posts()): while(have_posts()): the_post(); ?>
						<?php get_template_part('template-parts/post-home'); ?>
						<?php //get_template_part('template-parts/post', 'home'); ?>
					<?php endwhile; endif; ?>
   				</div>
   			</div>
   			<?php get_template_part('template-parts/pagination'); ?>
   		</div>
   	</section>

<?php get_footer(); ?>