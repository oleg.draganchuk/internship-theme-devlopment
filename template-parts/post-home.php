<div class="case">
    <div class="row">
        <div class="col-md-6 col-lg-6 col-xl-8 d-flex">
            <a href="<?php the_permalink();?>" class="img w-100 mb-3 mb-md-0" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/image_1.jpg);"></a>
        </div>
        <div class="col-md-6 col-lg-6 col-xl-4 d-flex">
            <div class="text w-100 pl-md-3">
                <span class="subheading"><?php the_category(); ?></span>
                <h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
                <ul class="media-social list-unstyled">
                    <li class="ftco-animate"><a href="http://facebook.com/share?url=<?php the_permalink();?>"><span class="icon-twitter"></span></a></li>
                    <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                    <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
                <div class="meta">
                    <p class="mb-0"><a href="#"><?php the_time('d-m-Y'); ?></a> | <a href="#"><?php time_to_read(); ?> min read</a></p>
                </div>
            </div>
        </div>
    </div>
</div>