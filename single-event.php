<?php get_header(); ?>
    
    <?php if (have_posts()): while(have_posts()): the_post(); ?>
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
          <div class="col-md-9 ftco-animate pb-5 text-center">
            <h1 class="mb-3 bread"><?php the_title(); ?></h1>
          </div>
        </div>
      </div>
    </section>

    <section class="container">
        <ul>
          <li>Start date: <?php the_field('start_date'); ?></li>
          <li>Agenda: <?php the_field('agenda'); ?></li>
          <li><?php the_field('location'); ?></li>
          <li><img src="<?php the_field('poster'); ?>"></li>
        </ul>
        <?php the_content(); ?>
    </section>

    <?php endwhile; endif; ?>
    
<?php get_footer(); ?>