<?php get_header(); ?>

  <?php get_template_part('template-parts/hero', 'archive');?>
	
	<section class="ftco-section bg-light">
      <div class="container">
        <div class="row d-flex">
          <?php if (have_posts()): while(have_posts()): the_post(); ?>
            <?php get_template_part('template-parts/event'); ?>
          <?php endwhile; endif; ?> 
        </div>
        <?php get_template_part('template-parts/pagination'); ?>
      </div>
    </section>


<?php get_footer(); ?>