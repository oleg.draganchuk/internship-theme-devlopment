<div class="hero-wrap" style="background-image: url('<?php the_field('background','options'); ?>');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-start" data-scrollax-parent="true">
        <div class="col-md-12 ftco-animate">
        <h1 class="mb-4 mb-md-0"><?php wp_title(); ?></h1>
        </div>
    </div>
    </div>
</div>